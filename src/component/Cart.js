import {useDispatch} from 'react-redux';
import {addCart} from './../redux/action/cartAction'

const Cart =()=>{
   const dispatch = useDispatch();

   return(
      <button onClick={()=>dispatch(addCart(1))}>
         Add Cart
      </button>
   ) 
}


export default Cart;