import React from "react";

class counter extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            count: 0,
            clickTime: 0
        };
    };
    componentDidUpdate(prevProps){
        if (this.state.clickTime !== prevProps.clickTime){
            if (this.state.clickTime === 5){
                this.setState({clickTime: 0});
                alert("you have change 5 times");
            }
        }
    }
    render(){
        return(
            <div>
                <h1>Counter</h1>
                <h2>Value is {this.state.count % 2 === 0 ? "even":"odd"}</h2>
                <h1>{this.state.count}</h1>
                <div>
                    <button
                        onClick={()=>
                            this.setState({
                                count: this.state.count+1,
                                clickTime: this.state.clickTime+1
                            })
                        }
                    >
                        Add
                    </button>
                    <button
                        onClick={()=>
                            this.setState({
                                count: this.state.count-1,
                                clickTime: this.state.clickTime+1
                            })
                        }
                    >
                        Min
                    </button>
                    <button
                        onClick={()=> this.setState({count: 0,})}
                    >
                        Reset
                    </button>
                </div>
            </div>
        )
    }
}

export default counter;