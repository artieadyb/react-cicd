import {useState} from 'react';
import {useDispatch} from 'react-redux';
import {changeColor} from './../redux/action/colorAction' 
const Color = ()=>{
   const [color, setColor]= useState("");
   const dispatch = useDispatch();

   return(
      <>
      <input value={color} onChange={(e)=>setColor(e.target.value)} type="text"/>
      <button onClick={()=>dispatch(changeColor(color))}>Klik</button>
      </>
   )
}

export default Color;