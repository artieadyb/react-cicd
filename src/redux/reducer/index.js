import {combineReducers} from 'redux';
import cartReducer from './cartReducer';
import colorReducer from './colorReducer';
import authReducer from './authReducer';
export default combineReducers({
   cart: cartReducer,
   color: colorReducer,
   auth: authReducer,
})
