const initialState = {
   colorName:"green"
}

const colorReducer = ( state = initialState, action)=>{
   switch(action.type){
      case "SET_COLOR_NAME":{
         return{
            ...state,
            colorName:action.payload
         };
      }
      default:{
         return{
            ...state
         }
      }
   }
}

export default colorReducer;