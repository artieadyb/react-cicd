import axios from 'axios';

const login = (dataInput) => {
   return async (dispatch) => {
      try{
         const data = await axios.post(
            `${process.env.REACT_APP_API_LOGIN}`,
            dataInput
            );
            dispatch({type:"SET_TOKEN", payload: data.token });
            localStorage.setItem('token', data.token);
      }  catch (err){
         console.log(err);
      }
   }
}