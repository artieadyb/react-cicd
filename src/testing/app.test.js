import { render, screen } from '@testing-library/react'
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk';
import App from './../App'

const initialState ={text:''}
const mockStore = configureStore([thunk])
let store = mockStore(initialState)

describe('testing for auth feature', ()=>{
   beforeEach(()=>{
      render ( 
         <Provider store={store}>
            <App/>
         </Provider>
      )
   })

   it('should be render Halaman Login', ()=>{
      const textLogin = screen.getByText('Halaman Login');
      expect (textLogin).toBeInTheDocument();
   })

   it('should be render Button Submit', ()=>{
      const buttonSubmit = screen.getByText('Submit');
      expect (buttonSubmit).toBeInTheDocument();
   })

   it('should be render test text', ()=>{
      const testElement = screen.getByTestId('test');
      expect (testElement).toBeInTheDocument();
   })
})