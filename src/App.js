import {useEffect, useState, useRef} from 'react';
import {useSelector, useDispatch} from 'react-redux';
// import axios from 'axios';
// import Cart from './component/Cart';
// import Color from './component/Color';
import {getListItem, login} from './redux/action';


const App =()=>{

   const dispatch = useDispatch()
   const globalStore = useSelector((data)=>data);

   // const getData = async ()=>{
      
   // };

   // console.log(globalStore);
   
   const [email, setEmail] = useState();
   const passwordRef = useRef();
   // console.log("iniuseref", passwordRef);

   const submitData = (e)=>{
      e.preventDefault();
      const passwordValue = passwordRef.current.value;
      // console.log(passwordValue);
      const dataInput = {
         email,
         password: passwordValue
      }
   }
   // console.log(email)
   console.log(localStorage.getItem('token'))
   useEffect(()=>{
      dispatch(getListItem())
      // getData();
   },[])

   // console.log(process.env.REACT_APP_BASE_API_URL)

   // const globalStore = useSelector((data)=>{
   //    console.log(data);
   //    return data;
   // })

   return(
         <>
         <h1>Halaman Login</h1>
         {/* <p style={{color:globalStore.color.colorName}}>Halaman Home, Jumlah Cart: {globalStore.cart.cartCount}</p>
         <Cart/> 
         <br/>
         <Color/> */}
         <form onSubmit={submitData}>
            <input 
            type="email" 
            value={email}
            onChange={(e)=>setEmail(e.target.value)}
            placeholder="Masukan Email"
            />
            <input 
            type="password" 
            ref={passwordRef}
            placeholder="Masukan Password"
            />
            <button data-testId="testgit " type="submit">Submit</button>
         </form>
         </>
      ) 
      
};

export default App;
